#!/usr/bin/env python3

# Create labels with qrcodes

import labels
import qrcode
from reportlab.graphics import shapes

def create_qrcode(data):
    qr = qrcode.QRCode(version=1, box_size=10, border=0)
    qr.add_data(data)
    qr.make(fit=True)
    return qr.make_image(fill='black', back_color='white')

def draw_label(label, width, height, data):
    s, p = data
    txt = shapes.String(4, 4, s, fontName="Helvetica", fontSize=12)
    img = create_qrcode(s)
    label.add(txt)
    imgpath = p
    img.save(imgpath)
    qw = 36
    qh = 36
    s = shapes.Image(10, height-qh-8, qw, qh, imgpath)
    label.add(s)

specs = labels.Specification(
            210,
            297,
            5,
            13,
            38,
            21,
            column_gap=0.1,
            row_gap=0.08,
            corner_radius=1,
            left_margin=10.1,
            top_margin=11.1
        )

sheet = labels.Sheet(specs, draw_label, border=False)

l = [('/lab/B%d' % x, '/tmp/qrcodes/%d.png' % x) for x in range(4 *5 * 13, 5 * 5 * 13)]
sheet.add_labels(l)

# Save the file and we are done.
sheet.save('basic.pdf')
print("{0:d} label(s) output on {1:d} page(s).".format(sheet.label_count, sheet.page_count))

